const Repository = require('./repository')
const {Task} = require('../../models')

class TaskRepository extends Repository {
    /**
     * TaskRepository constructor.
     */
    constructor() {
        super(Task)
    }
}

module.exports = TaskRepository
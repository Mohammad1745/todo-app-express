const Service = require('../service')
const TaskRepository = require('../../repositories/task_repository')

class TaskService extends Service {
    /**
     * TaskService constructor.
     */
    constructor() {
        super(new TaskRepository)
    }

    /**
     * @param {BufferSource} userId
     * @param {Object} data
     * */
    taskDataFormatter = (userId, data) => {
        return userId && data ?
        {
            userId: userId,
            title: data.title,
            description: data.description
        } : {}
    }
}

module.exports = TaskService
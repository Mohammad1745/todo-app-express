const ResponseService = require('../response_service')
const TaskService = require('../base/task_service')

class TodoService extends ResponseService {

    /**
     * TaskService constructor.
     */
    constructor() {
        super()
        this.taskService = new TaskService
    }

    /**
     * @return {Object}
     */
    index = async request => {
        try {
            let tasks = await this.taskService.findAllWhere({where:{userId: request.user.id}})
            tasks.map( element => element.description = element.description.substring(0,40)+'...')
            return this.response(tasks).success()
        } catch (e) {
            console.log(e.message)
            return this.response().error(e.message)
        }
    }

    /**
     * @param {Object} request
     * @return {Object}
     */
    store = async request => {
        try {
            const task = await this.taskService.create(this.taskService.taskDataFormatter(request.user.id, request.body))
            return this.response(task).success('Task Created Successfully')
        } catch (e) {
            return this.response().error(e.message)
        }
    }

    /**
     * @param {Object} request
     * @return {Object}
     */
    read = async request => {
        try {
            const task = await this.taskService.findOneWhere({where: {id: Number(request.params.id), userId: request.user.id}})
            return this.response(task).success()
        } catch (e) {
            return this.response().error(e.message)
        }
    }

    /**
     * @param {Object} request
     * @return {Object}
     */
    update = async request => {
        try {
            const task = await this.taskService.findOneWhere({where: {id: Number(request.params.id), userId: request.user.id}})
            if (!task){
                return this.response().error('Task Doesn\'t Exists')
            }
            await this.taskService.updateWhere({where:{id: Number(task.id)}}, this.taskService.taskDataFormatter(request.user.id, request.body))
            return this.response().success('Task Updated Successfully')
        } catch (e) {
            return this.response().error(e.message)
        }
    }

    /**
     * @param {Object} request
     * @return {Object}
     */
    delete = async request => {
        try {
            const task = await this.taskService.findOneWhere({where: {id: Number(request.params.id), userId: request.user.id}})
            if (!task){
                return this.response().error('Task Doesn\'t Exists')
            }
            await this.taskService.destroy({where:{id: Number(task.id)}})
            return this.response().success('Task Deleted Successfully')
        } catch (e) {
            return this.response().error(e.message)
        }
    }
}

module.exports = TodoService